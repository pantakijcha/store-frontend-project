import Vue from "vue";
import Vuex from "vuex";

import { cache } from "@/store/cache.module";

const initialState = {
  isGlobalLoading: false,
};

Vue.use(Vuex);

export default new Vuex.Store({
  state: initialState,
  actions: {
    SetGlobalLoading({ commit }, value) {
      commit("SetLoadingState", value);
    },
  },
  mutations: {
    SetLoadingState(state, value) {
      state.isGlobalLoading = value;
    },
  },
  modules: { cache },
});
