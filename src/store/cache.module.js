import StaticValueService from "@/services/StaticValues";

const initialState = {
  db: openDatabase("cache", "1.0", "Cache data from web service", 1),
  table: {
    descriptions: "DESCRIPTIONS",
  },
};

export const cache = {
  namespaced: true,
  state: initialState,
  actions: {
    ArrayWraper(vuexDummy, resultObject) {
      let result = [];
      for (let i = 0; i < resultObject.rows.length; i++) {
        result.push(resultObject.rows.item(i));
      }
      return result;
    },
    async InitialData({ state }) {
      let data = await StaticValueService.GetInitialData();

      // Prepare table
      state.db.transaction((transaction) => {
        transaction.executeSql(
          `CREATE TABLE IF NOT EXISTS ${state.table.descriptions} (KEY, NAME, CATEGORY, LOCALE)`
        );
        transaction.executeSql(`DELETE FROM ${state.table.descriptions}`);
      });

      // Insert data
      for (let des of data.descriptions) {
        state.db.transaction((transaction) => {
          transaction.executeSql(
            `INSERT INTO ${state.table.descriptions} (KEY, NAME, CATEGORY, LOCALE) VALUES (?, ?, ?, ?)`,
            [des.key, des.name, des.category, des.locale]
          );
        });
      }
    },
    GetBillStatus({ state, dispatch }, locale) {
      return new Promise(function (resolve, reject) {
        try {
          state.db.readTransaction((transaction) => {
            transaction.executeSql(
              `SELECT * FROM ${state.table.descriptions} WHERE CATEGORY='BILL_STATUS' AND LOCALE=?`,
              [locale],
              function (tx, result) {
                resolve(dispatch("ArrayWraper", result));
              }
            );
          });
        } catch (e) {
          reject(e);
        }
      });
    },
  },
};
