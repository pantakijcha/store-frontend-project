import axios from "axios";
import { env } from "@/env";

class UnitService {
  async GetUnits(locale) {
    let res = await axios.get(`${env.apiUrl}/units?locale=${locale}`);
    return res.data;
  }
}

export default new UnitService();
