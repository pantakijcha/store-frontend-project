import axios from "axios";
import { env } from "@/env";

class BillService {
  async GetById(id, locale) {
    let res = await axios.get(`${env.apiUrl}/bills?id=${id}&locale=${locale}`);
    return res.data;
  }

  async AddBill(bill) {
    let res = await axios.post(`${env.apiUrl}/bills`, bill);
    return res.data;
  }

  async UpdateBill(bill) {
    let res = await axios.put(`${env.apiUrl}/bills/${bill.id}`, bill);
    return res.data;
  }

  async Search(locale, groupId, customerId, status) {
    let res = await axios.post(`${env.apiUrl}/bills/search`, {
      locale,
      groupId,
      customerId,
      status,
    });
    return res.data;
  }

  async Close(bills) {
    let res = await axios.post(`${env.apiUrl}/bills/close`, {
      bills,
    });
    return res.data;
  }
}

export default new BillService();
