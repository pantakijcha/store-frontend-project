import axios from "axios";
import { env } from "@/env";

class BrandService {
  async GetBrands(locale) {
    let res = await axios.get(`${env.apiUrl}/brands?locale=${locale}`);
    return res.data;
  }

  async FillterWithCategories(locale, categoryIds) {
    let res = await axios.post(`${env.apiUrl}/brands/categories`, {
      locale,
      categoryIds,
    });
    return res.data;
  }
}

export default new BrandService();
