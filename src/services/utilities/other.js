class OtherUtility {
  useFluid(breakpoint) {
    switch (breakpoint) {
      case "xs":
      case "sm":
      case "md":
      case "lg":
        return true;
      case "xl":
      default:
        return false;
    }
  }
}

export default new OtherUtility();
