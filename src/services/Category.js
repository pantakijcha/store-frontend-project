import axios from "axios";
import { env } from "@/env";

class CategoryService {
  async GetCategories(locale) {
    let res = await axios.get(`${env.apiUrl}/categories?locale=${locale}`);
    return res.data;
  }
}

export default new CategoryService();
