import axios from "axios";
import { env } from "@/env";

class ProductService {
  async Create(product, locale) {
    let res = await axios.post(`${env.apiUrl}/products`, {
      locale,
      product,
    });
    return res.data;
  }

  async Update(product, locale) {
    let res = await axios.put(`${env.apiUrl}/products/${product.id}`, {
      locale,
      product,
    });
    return res.data;
  }

  async Search(locale, categories, brands, search) {
    let res = await axios.post(`${env.apiUrl}/products/search`, {
      locale,
      categories,
      brands,
      search,
    });
    return res.data;
  }

  async GetById(id, locale) {
    let res = await axios.get(
      `${env.apiUrl}/products?id=${id}&locale=${locale}`
    );
    return res.data;
  }
}

export default new ProductService();
