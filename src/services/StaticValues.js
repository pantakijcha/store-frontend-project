import axios from "axios";
import { env } from "@/env";

class StaticValueService {
  async GetInitialData() {
    let res = await axios.get(`${env.apiUrl}/descriptions`);

    return res.data;
  }
}

export default new StaticValueService();
