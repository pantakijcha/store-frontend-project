import axios from "axios";
import { env } from "@/env";

class CustomerService {
  async GetAll(locale, groupId) {
    let res = await axios.get(
      `${env.apiUrl}/customers?locale=${locale}&groupId=${groupId}`
    );
    return res.data;
  }

  async GetAllGroup(locale) {
    let res = await axios.get(`${env.apiUrl}/groups?locale=${locale}`);
    return res.data;
  }
}

export default new CustomerService();
