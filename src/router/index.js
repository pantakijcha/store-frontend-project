import Vue from "vue";
import VueRouter from "vue-router";
import Dashboard from "@/Pages/Dashboard/index";

Vue.use(VueRouter);

const routes = [
  {
    path: "/dashboard/",
    component: Dashboard,
    children: [
      {
        name: "Product",
        path: "/product/",
        meta: {
          auth: true,
        },
        // lazy-loaded
        component: () => import("@/Pages/Dashboard/Product"),
      },
      {
        name: "Bill Search",
        path: "/bill-search/",
        meta: {
          auth: true,
        },
        // lazy-loaded
        component: () => import("@/Pages/Dashboard/BillSearch"),
      },
      {
        name: "Bill Close",
        path: "/bill-close/",
        meta: {
          auth: true,
        },
        // lazy-loaded
        component: () => import("@/Pages/Dashboard/BillClose"),
      },
      {
        name: "Bill",
        path: "/bill/",
        meta: {
          auth: true,
        },
        // lazy-loaded
        component: () => import("@/Pages/Dashboard/Bill"),
      },
      {
        name: "Print Bills",
        path: "/print-bills/",
        meta: {
          auth: true,
        },
        // lazy-loaded
        component: () => import("@/Pages/Dashboard/PrintBills"),
      },
    ],
  },
  {
    path: "/403",
    name: "403",
    meta: {
      auth: false,
    },
    component: () => import("@/Pages/403"),
  },
  {
    path: "*",
    name: "404",
    meta: {
      auth: false,
    },
    component: () => import("@/Pages/404"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes: routes,
});

export default router;
